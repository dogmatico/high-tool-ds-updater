'use strict';

describe('RowModification behaviour.', () => {
  const RowModification = require('../lib/RowModification.js');
  const {Trie, HierarchicalTrie} = require('../lib/DataStructures.js');
  const {HermiteSpline} = require('../lib/LinearAlgebra.js');

  const fullValues = {
    terminalValue : 10,
    startTime : 1,
    timeSeries : [[1,3], [3,3]],
    spread : {
      trie : new HierarchicalTrie('EU28', [
        ['EU28', 2],
        ['reg1', 1],
        ['reg2', 1.5]
      ]),
      modifiedNodes : new Trie([
        ['reg2', {
          timeSeries : new HermiteSpline([[1,2], [2,2]]),
          terminalValue : 2
        }]
      ]),
      AffineMap : [1, 3, 1, 3]
    }
  };


  it('Should be defined', () => {
    expect(RowModification).toBeDefined();
  });

  describe('Constructor parameters test.', () => {
    it('Should create a this.value.terminalValue if first argument is a Number', () => {
      const fun = new RowModification(0, 'target');
      expect(fun.value.terminalValue).toEqual(0);
    });

    it('If first argument is an object with a terminalValue, it should be copied to this.value.terminalValue ', () => {
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.value.terminalValue).toEqual(0);
      const fun2 = new RowModification({terminalValue : 2}, 'target');
      expect(fun2.value.terminalValue).toEqual(2);
    });

    it('If first argument is an object with a timeSeries array, this.value.timeSeries must be the related Hermite Spline', () => {
      const {HermiteSpline} = require('../lib/LinearAlgebra.js');
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.value.timeSeries).toBe(null);
      const fun2 = new RowModification({terminalValue : 0, timeSeries : [[1,2], [3,2]]}, 'target');
      expect(fun2.value.timeSeries instanceof HermiteSpline).toBe(true);
    });

    it('If first argument is an object with a spread object, it must be pointed by this.value.spread', () => {
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.value.spread).toBe(null);

      const fun2 = new RowModification(fullValues, 'target');
      expect(fun2.value.spread).not.toBe(null);
    });


    it('Should throw a TypeError if no valid terminalValue is provided', () => {
      const failureMsg = 'Constructor requires a number as a first argument or an object with a number in terminalValue property';
      expect(() => {new RowModification()}).toThrowError(TypeError, failureMsg);
      expect(() => {new RowModification(0), 'target'}).not.toThrowError(TypeError, failureMsg);
      expect(() => {new RowModification({terminalValue : 0}, 'target')}).not.toThrowError(TypeError, failureMsg);
    });

    it('Should throw a TypeError if no target is not given or is of invalid type', () => {
      const missingTargetMsg = 'Missing target or not a string';
      expect(() => { new RowModification({terminalValue : 0}) }).toThrowError(TypeError, missingTargetMsg);
      expect(() => { new RowModification({terminalValue : 0}, {}) }).toThrowError(TypeError, missingTargetMsg);
      expect(() => { new RowModification({terminalValue : 0}, 'target') }).not.toThrowError(TypeError, missingTargetMsg);
    });

    it('Should copy target argument to this.target', () => {
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.target).toEqual('target');
    });


    it('Should default this.isRelative to true if no {isRelative} is given as third argument', () => {
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.isRelative).toBe(true);
    });

    it('Should copy {isRelative} property of third argument is given', () => {
      const fun = new RowModification({terminalValue : 0}, 'target', {isRelative : true});
      expect(fun.isRelative).toBe(true);
      const fun2 = new RowModification({terminalValue : 0}, 'target', {isRelative : false});
      expect(fun2.isRelative).toBe(false);
    });

    it('Should copy {timeColumn} property of third argument is given or default to null', () => {
      const fun = new RowModification({terminalValue : 0}, 'target', {isRelative : true});
      expect(fun.timeColumn).toBe(null);
      const fun2 = new RowModification({terminalValue : 0}, 'target', {isRelative : true, timeColumn : 'timeColumn'});
      expect(fun2.timeColumn).toEqual('timeColumn');
    });

    it('Should copy {geoColumn} property of third argument is given or default to null', () => {
      const fun = new RowModification({terminalValue : 0}, 'target', {isRelative : true});
      expect(fun.geoColumn).toBe(null);
      const fun2 = new RowModification({terminalValue : 0}, 'target', {isRelative : true, geoColumn : 'geoColumn'});
      expect(fun2.geoColumn).toEqual('geoColumn');
    });

    it('Should default this.geoIdToCodes to the identity map if not {geoIdToCodes} property of third argument is given or if not a function', () => {
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.geoIdToCodes('code')).toEqual('code');
      const fun2 = new RowModification({terminalValue : 0}, 'target', {geoIdToCodes : {}});
      expect(fun2.geoIdToCodes('code')).toEqual('code');
    });

    it('If third argument {geoIdToCodes} is a function, point it to this.geoIdToCodes', () => {
      const fun1 = new RowModification({terminalValue : 0}, 'target', {geoIdToCodes : (code) => { return 1 + code; }});
      expect(fun1.geoIdToCodes('code')).toEqual('1code');
      const fun2 = new RowModification({terminalValue : 0}, 'target', {geoIdToCodes : () => { return 1; }});
      expect(fun2.geoIdToCodes('code')).toEqual(1);
    });

    it('Should default this.filter to null if options.filter is not present', () => {
      const fun = new RowModification({terminalValue : 0}, 'target');
      expect(fun.filter).toBe(null);
    });

    it('Should copy, if present, options.filter to this.filter and transform entries iterables in sets', () => {
      const fun = new RowModification({terminalValue : 0}, 'target', {
        filter : {
          'key' : [1, 2, 3]
        }
      });
      expect(fun.filter).toBeDefined();
      expect(fun.filter.key.has(1)).toEqual(true);
      expect(fun.filter.key.has(5)).not.toEqual(true);
    });

    describe('Use of this.filter to check if a row must be modified.', () => {
      const row = {
        'column_1' : 1,
        'column_2' : 2
      };

      it('Should modifiy a row if no filter is given', () => {
        const fun = new RowModification({terminalValue : 0}, 'target');
        expect(fun.__modifyRow(row)).toBe(true);
      });

      it('Should not modifiy a row if a filter points to missing row', () => {
        const fun = new RowModification({terminalValue : 0}, 'target', {
          filter : {
            'column_3' : [1, 2, 3]
          }
        });
        expect(fun.__modifyRow(row)).toBe(false);
      });

      it('Should not modifiy a row if a a row does not pass all the filters', () => {
        const fun = new RowModification({terminalValue : 0}, 'target', {
          filter : {
            'column_1' : [1],
            'column_2' : [1]
          }
        });
        expect(fun.__modifyRow(row)).toBe(false);
      });

      it('Should modifiy a row if a a row pass all the filters', () => {
        const fun = new RowModification({terminalValue : 0}, 'target', {
          filter : {
            'column_1' : [1],
            'column_2' : [1, 2]
          }
        });
        expect(fun.__modifyRow(row)).toBe(true);
      });

    });

    describe('exec method behaviour.', () => {
      it('Should throw a TypeError if target column is not present', () => {
        const missingTargetInRowMsg = 'The provided row does not has the target column'
        const fun = new RowModification({terminalValue : 2}, 'target');
        const row = {
          'target' : 0.5,
          'column_1' : 1,
          'column_2' : 2
        };

        expect(() => { fun.exec(row) }).not.toThrowError(TypeError, missingTargetInRowMsg);
        delete row.target;
        expect(() => { fun.exec(row) }).toThrowError(TypeError, missingTargetInRowMsg);
      });

      it('Should return the modified row (same properties)', () => {
        const fun = new RowModification({terminalValue : 2}, 'target');
        const row = {
          'target' : 0.5,
          'column_1' : 1,
          'column_2' : 2
        };
        const retRow = fun.exec(row);
        expect(retRow.target).toBeDefined();
        expect(retRow.column_1).toBeDefined();
        expect(retRow.column_2).toBeDefined();
      })

      it('Should multiply the target value if isRelative', () => {
        const fun = new RowModification({terminalValue : 2}, 'target', {isRelative : true});
        const row = {
          'target' : 0.5
        };
        expect(fun.exec(row).target).toEqual(1);
      })

      it('Should replace the target value if not isRelative', () => {
        const fun = new RowModification({terminalValue : 2}, 'target', {isRelative : false});
        const row = {
          'target' : 0.5
        };
        expect(fun.exec(row).target).toEqual(2);
      });

      it('Should not use this.values.timeSeries if timeColumn is not defined', () => {
        const fun = new RowModification({terminalValue : 2, timeSeries : [[1,3], [3,3]]}, 'target', {isRelative : false});
        const row = {
          'target' : 0.5,
          'time' : 1
        };
        expect(fun.exec(row).target).toEqual(2);
      });

      it('Should not use this.values.timeSeries if timeColumn is not in the row', () => {
        const fun = new RowModification({terminalValue : 2, timeSeries : [[1,3], [3,3]]}, 'target', {isRelative : false, timeColumn : 'wrongColumn'});
        const row = {
          'target' : 0.5,
          'time' : 1
        };
        expect(fun.exec(row).target).toEqual(2);
      });

      it('Should only use this.values.timeSeries if timeColumn is defined and present in the row and no geoColumn', () => {
        const fun = new RowModification({terminalValue : 2, timeSeries : [[1,3], [3,3]]}, 'target', {isRelative : false, timeColumn : 'time'});
        const row = {
          'target' : 1,
          'time' : 1
        };
        expect(fun.exec(row).target).toEqual(3);

        const fun2 = new RowModification({terminalValue : 2, timeSeries : [[1,3], [3,3]]}, 'target', {isRelative: false, timeColumn : 'time'});
        expect(fun2.exec(row).target).toEqual(3);
      });

      it('Should only use this.values.spread if geoColumn is defined and present in the row and no timeColumn', () => {
        const fun = new RowModification(fullValues, 'target', {isRelative : false, geoColumn : 'geo'});
        const row0 = {
          'geo' : 'reg1',
          'target' : 9
        };
        const row1 = {
          'geo' : 'reg2',
          'target' : 1
        };
        expect(fun.exec(row0).target).toEqual(1);

        const fun2 = new RowModification(fullValues, 'target', {isRelative : false, geoColumn : 'geo'});
        expect(fun2.exec(row1).target).toEqual(2);
      });

      it('Should combine time and geo columns if both are present', () => {
        const fun = new RowModification(fullValues, 'target', {isRelative : false, geoColumn : 'geo', timeColumn : 'time'});
        const row0 = {
          'geo' : 'reg1',
          'time' : 1,
          'target' : 9
        };
        const row1 = {
          'geo' : 'reg2',
          'time' : 2,
          'target' : 1
        };
        expect(fun.exec(row0).target).toEqual(1);

        const fun2 = new RowModification(fullValues, 'target', {isRelative : false, geoColumn : 'geo', timeColumn : 'time'});
        expect(fun2.exec(row1).target).toEqual(2);
      });

      it('Should return the row unmodified if using time and time is less than value.startYear', () => {

        const fun = new RowModification(fullValues, 'target', {isRelative : false, timeColumn : 'time'});
        const row0 = {
          'target' : 100,
          'time' : 0.5
        };

        const row1 = {
          'target' : 100,
          'time' : 1
        };


        expect(fun.exec(row0).target).toEqual(100);
        expect(fun.exec(row1).target).toEqual(3);

      })

    });

  });

});
