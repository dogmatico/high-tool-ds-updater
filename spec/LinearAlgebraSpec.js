'use strict';

describe('Hermite Polynomial', () => {
  const {HermitePolynomial} = require('../lib/LinearAlgebra.js');

  xit('Should require an array of points as argument', () => {
    expect(() => {new HermitePolynomial()}).toThrowError(TypeError, 'Invalid argument. Must provide an array of points.');
    expect(() => {new HermitePolynomial(1, 2, 3, 4)}).toThrowError(TypeError, 'Invalid argument. Must provide an array of points.');
  });

});
