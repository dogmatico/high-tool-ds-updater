'use strict';


describe('Rescale bounds', () => {


  describe('Static method rescale', () => {
    const {Rescale} = require('../lib/RescaleFunctions.js');

    it('Should be a static method', () => {
      expect(() => {new Rescale.rescale()}).toThrowError(TypeError, 'Rescale.rescale is not a constructor');
    })

    it('Requires 3 arguments (x, minX, maxX)', () => {
      expect(() => {Rescale.rescale()}).toThrowError(TypeError, 'Missing arguments');
    });

    it('Should throw an error if trying to compute an out of bounds value', () => {
      expect(() => { Rescale.rescale(-1, 0, 2); }).toThrowError(TypeError, 'Out of bounds argument');
      expect(() => { Rescale.rescale(3, 0, 2); }).toThrowError(TypeError, 'Out of bounds argument');
    });

    it('Should map [minX, maxX] to [0, 1] if no additional arguments given', () => {
      expect(Rescale.rescale(1, 0, 2)).toEqual(0.5);
    });

    it('Should map [minX, maxX] to [minY, maxY] if 5 arguments are given', () => {
      expect(Rescale.rescale(1, 0, 2, 2, 4)).toEqual(3);
    });

    it('Should return the lower bound if |minY, maxY| < 1e8', () => {
      expect(Rescale.rescale(0, 0, 1, 1e-9, 2e-9)).toEqual(1e-9);
    });

    it('Should return the lower bound if |minY, maxY| < tol = 1e-5', () => {
      expect(Rescale.rescale(0, 0, 1, 1e-9, 2e-9, 1e-5)).toEqual(1e-9);
      expect(Rescale.rescale(0.5, 0, 1, 0, 1e-3, 1e-5)).not.toEqual(1e-3);
    });
  });

  describe('TimeAndGeoRescale', () => {
    const {TimeAndGeoRescale} = require('../lib/RescaleFunctions.js');

    it('Should be a constructor', () => {
      expect(() => { TimeAndGeoRescale(); }).toThrowError(TypeError, 'Class constructor TimeAndGeoRescale cannot be invoked without \'new\'');
      expect(() => { new TimeAndGeoRescale(); }).not.toThrowError(TypeError, 'Class constructor TimeAndGeoRescale cannot be invoked without \'new\'');
    });

    it('Requires a trie as a first parameter of the constructor', () => {
      expect(() => { new TimeAndGeoRescale(); }).toThrowError(TypeError, 'Provide a reference trajectory as the first argument of the constructor');
      expect(() => { new TimeAndGeoRescale({}); }).toThrowError(TypeError, 'Provide a HierarchicalTrie as the second argument of the constructor');
    });
  })


});
