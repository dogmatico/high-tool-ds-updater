'use strict';

describe('TransformModelRunToArguments behaviour.', () => {
  const transformModelRunToArguments = require('../lib/TransformModelRunToArguments.js');
  const HermiteSpline = require('../lib/LinearAlgebra.js');
  const validTries = {
    'trie0' : [
      ['EU28', 'result'],
      ['key1', 1]
    ]
  };

  var completeExample;

  beforeEach(() => {
    completeExample = {
      "label": "Change of access/ egress time for passenger rail",
      "target": {
        "__is_relative": true,
        "__parameters": [
          "i_pd_core_lever_ae_time"
        ],
        "__dimensions": {
          "mode": [
            "2"
          ]
        }
      },
      "uuid": "TPM_TPM_4_i_pd_core_lever_ae_time_2",
      "values": {
        "terminalValue": 0.97,
        "representationaData": {
          "timeSeries": [[1,1], [2,2]],
          "spread": {
            "modifiedNodes": [
              {
                "key": "NL",
                "value": 0.95,
                "timeSeries": [
                  [
                    2010,
                    1,
                    0
                  ],
                  [
                    2015,
                    0.95,
                    0
                  ],
                  [
                    2050,
                    0.95,
                    0
                  ]
                ]
              }
            ],
            "transformation": [
              1,
              1,
              0.8,
              1
            ],
            "id": "baseline"
          }
        },
        "startYear": 2015
      },
      "min": 0.8,
      "max": 1,
      "initialValue": 1
      }
    }
  );

  it('Should return an object with keys value, target, options', () => {
    const retObj = transformModelRunToArguments(completeExample);
    expect(retObj.value).toBeDefined();
    expect(retObj.target).toBeDefined();
    expect(retObj.options).toBeDefined();
  });

  it('Should send a TypeError if target.__parameters is undefined or has more than one element', () => {
    const errorMsg = 'ModelRuns must have include an array in target.__parameters with a single element';
    const wrongArgs1 = {
      "target": {
        "__is_relative": true,
        "__dimensions": {
          "mode": [
            "2"
          ]
        }
      }
    };

    const wrongArgs2 = {
      "target": {
        "__is_relative": true,
        "__parameters": [
          "i_pd_core_lever_ae_time",
          "i_pd_core_lever_ae_time2"
        ],
        "__dimensions": {
          "mode": [
            "2"
          ]
        }
      }
    };

    expect(() => { transformModelRunToArguments(wrongArgs1); }).toThrowError(TypeError, errorMsg);
    expect(() => { transformModelRunToArguments(wrongArgs2); }).toThrowError(TypeError, errorMsg);
    expect(() => { transformModelRunToArguments(completeExample); }).not.toThrowError(TypeError, errorMsg);
  });

  it('Should send a TypeError if target.__parameters is undefined or has more than one element', () => {
    const errorMsg = 'Wrong arguments: target.__parameters[0] must be a non-empty string';

    const wrongArgs1 = {
      "target": {
        "__is_relative": true,
        "__parameters": [
          {}
        ],
        "__dimensions": {
          "mode": [
            "2"
          ]
        }
      }
    };

    const wrongArgs2 = {
      "target": {
        "__is_relative": true,
        "__parameters": [
          ''
        ],
        "__dimensions": {
          "mode": [
            "2"
          ]
        }
      }
    };

    expect(() => { transformModelRunToArguments(wrongArgs1); }).toThrowError(TypeError, errorMsg);
    expect(() => { transformModelRunToArguments(wrongArgs2); }).toThrowError(TypeError, errorMsg);
    expect(() => { transformModelRunToArguments(completeExample); }).not.toThrowError(TypeError, errorMsg);
  });

  it('Should set target property to target.__parameters[0]', () => {
    expect(transformModelRunToArguments(completeExample).target).toEqual('i_pd_core_lever_ae_time');
  });

  it('Should copy, if present, target.__is_relative to options.isRelative', () => {
    expect(transformModelRunToArguments(completeExample).options.isRelative).toBe(true);

    completeExample.target.is_relative = false;
    expect(transformModelRunToArguments(completeExample).options.isRelative).toBe(true);

    delete completeExample.target.__is_relative;
    expect(transformModelRunToArguments(completeExample).options.isRelative).not.toBeDefined();
  });

  it('Should add options.timeColumn argument if the target parameter has a valid time column', () => {
    const targetDimensionsDict = {
      'i_pd_core_lever_ae_time' : {
        dimensions : new Set(['time', 'target0'])
      },
      'targetInvalid' : {
        dimensions : new Set(['invalidTime', 'target0'])
      }
    };

    const output0 = transformModelRunToArguments(completeExample, targetDimensionsDict);

    completeExample.target.__parameters[0] = 'targetInvalid';
    const output1 = transformModelRunToArguments(completeExample, targetDimensionsDict);

    expect(output0.options.timeColumn).toEqual('time_id');
    expect(output1.options.timeColumn).not.toBeDefined();

  });

  it('Should add options.geoColumn argument if the target parameter has a valid geoColumn', () => {
    const targetDimensionsDict = {
      'i_pd_core_lever_ae_time' : {
        dimensions : new Set(['time', 'region_eu'])
      },
      'targetInvalid' : {
        dimensions : new Set(['invalidTime', 'target0'])
      }
    };

    const output0 = transformModelRunToArguments(completeExample, targetDimensionsDict);

    completeExample.target.__parameters[0] = 'targetInvalid';
    const output1 = transformModelRunToArguments(completeExample, targetDimensionsDict);

    expect(output0.options.geoColumn).toEqual('region_eu_id');
    expect(output1.options.geoColumn).not.toBeDefined();
  });

  it('Should copy target.__dimensions to options.filter as object keys = columnName + \'_id\' and value = array of values', () => {
    const output0 = transformModelRunToArguments(completeExample);
    expect(output0.options.filter).toEqual({'mode_id' : ['2']});

    completeExample.target.__dimensions = {
      'column1' : [1,2,3],
      'column2' : [3,4,5]
    };
    const output1 = transformModelRunToArguments(completeExample);
    expect(output1.options.filter.column1_id).toEqual([1, 2, 3]);
    expect(output1.options.filter.column2_id).toEqual([3, 4, 5]);
  });

  it('Should throw a TypeError of missing modelRunParameter.values.terminalValue', () => {
    delete completeExample.values.terminalValue;
    expect(() => { transformModelRunToArguments(completeExample); }).toThrowError(TypeError, 'Missing values. Cannot transform the given modelRun.');
  });

  it('Should copy values.terminalValue to value.terminalValue', () => {
    const retObj = transformModelRunToArguments(completeExample);
    expect(retObj.value.terminalValue).toEqual(0.97);
  });

  it('Should copy values.firstYear to value.firstYear. If not present, set it to 2015', () => {
    completeExample.values.startYear = 2030;
    expect(transformModelRunToArguments(completeExample).value.startTime).toEqual(2030);

    delete completeExample.values.startYear;
    expect(transformModelRunToArguments(completeExample).value.startTime).toEqual(2015);
  })

  it('Should copy values.representationaData.timeSeries to value.timeSeries if a non empty array', () => {
    expect(transformModelRunToArguments(completeExample).value.timeSeries).toEqual([[1,1], [2,2]]);

    completeExample.values.representationaData.timeSeries = [];
    expect(transformModelRunToArguments(completeExample).value.timeSeries).not.toBeDefined();

    delete completeExample.values.representationaData.timeSeries;
    expect(transformModelRunToArguments(completeExample).value.timeSeries).not.toBeDefined();
  });

  describe('Copy of values.representationaData.spread.', () => {
    it('Should copy the HierarchicalTrie trie identified as trie.id, if different of \'baseline\', non empty and valid, to value.spread.trie', () => {
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.trie).not.toBeDefined();

      completeExample.values.representationaData.spread.id = 'trie0';
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.trie.get('key1')).toEqual(1);

      completeExample.values.representationaData.spread.id = 'trie1';
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.trie).not.toBeDefined();

      completeExample.values.representationaData.spread = {};
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread).not.toBeDefined();

      delete completeExample.values.representationaData.spread;
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread).not.toBeDefined();
    });

    it('The copied HierarchicalTrie should have EU28 as rootId', () => {
      completeExample.values.representationaData.spread.id = 'trie0';
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.trie.get('invalidKey')).toEqual('result');
    });

    it('Should copy transformation to value.spread.AffineMap if present and trie.id !== baseline', () => {
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.AffineMap).not.toBeDefined();

      completeExample.values.representationaData.spread.id = 'trie0';
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.AffineMap).toEqual([1, 1, 0.8, 1]);

      delete completeExample.values.representationaData.spread.transformation;
      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.AffineMap).not.toBeDefined();
    });

    it('Should create a HierarchicalTrie trie with modified nodes to value.spread.modifiedNodes. Each code should have {timeSeries, terminalValue}', () => {
      const definedCountry = transformModelRunToArguments(completeExample, {}, validTries).value.spread.modifiedNodes.get('NL');
      expect(definedCountry.timeSeries).toBeDefined();
      expect(definedCountry.terminalValue).toEqual(0.95);

      const definedSubCountry = transformModelRunToArguments(completeExample, {}, validTries).value.spread.modifiedNodes.get('NL1');
      expect(definedCountry.timeSeries).toBeDefined();
      expect(definedCountry.terminalValue).toEqual(0.95);


      expect(transformModelRunToArguments(completeExample, {}, validTries).value.spread.modifiedNodes.get('invalidKey')).toBe(false);
    });
  });


});
