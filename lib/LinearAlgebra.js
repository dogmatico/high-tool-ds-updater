'use strict';

function RescaleBounds(min, max, lowerBound, upperBound) {
  if (arguments.length < 4) {
    throw new TypeError('Missing arguments');
  }
  return (value) => {
    if (value < min || value > max) {
      throw new TypeError('Out of bounds argument');
    }
    return lowerBound + (value - min)*(upperBound - lowerBound)/(max - min);
  };
}

function escaleBoundsWRTValue(x, refVal, minX, maxX, minY = 0, maxY = 1, tol = 1e-8) {
  const [min, max, lowB, upB] = (x < refVal ? [minX, refVal, minY, refVal] : [refVal, maxX, refVal, maxY]);
  if (Math.abs(lowB - upB) < tol) {
    return x;
  }
  return lowb + (x - min)*(upB - lowB) / (max - min);
}

function HermitePolynomial(interpolationPoints) {
    if (!Array.isArray(interpolationPoints)) {
        return new TypeError('Invalid argument. Must provide an array of points.');
    }
    if (interpolationPoints.length < 1) {
        return new TypeError('Invalid argument. Must provide at least a point.');
    }
    this._coefficients = interpolationPoints.map(function (point) {
        return [parseFloat(point[0], 10), parseFloat(point[1], 10)];
    });
    this._coefficients.sort(function (a, b) {
        return (a[0] - b[0]);
    });
    this._interpolPts = interpolationPoints;
    function factorial(n) {
        var res = 1;
        if (n <= 1) {
            return res;
        }
        for (var i = 1; i <= n; i += 1) {
            res *= i;
        }
        return res;
    }
    for (var i = 1, ln = this._coefficients.length; i < ln; i += 1) {
        for (var j = ln - 1; j >= i; j -= 1) {
            if (this._coefficients[j][0] === this._coefficients[j - i][0]) {
                this._coefficients[j][1] = this._coefficients[j][1] / factorial(1 + j - i);
            }
            else {
                var dx = this._coefficients[j][0] - this._coefficients[j - i][0];
                this._coefficients[j][1] = (this._coefficients[j][1] - this._coefficients[j - 1][1]) / dx;
            }
        }
    }
}
HermitePolynomial.prototype.evaluate = function (x) {
    var n = this._coefficients.length;
    var res = this._coefficients[n - 1][1];
    for (var i = n - 2; i >= 0; i -= 1) {
        res = res * (x - this._coefficients[i][0]) + this._coefficients[i][1];
    }
    return res;
};
HermitePolynomial.prototype.derivative = function (x) {
    var n = this._coefficients.length;
    var Pn = this._coefficients[n - 1][1];
    var dPn = 0;
    for (var i = n - 2; i >= 0; i -= 1) {
        dPn = dPn = dPn * (x - this._coefficients[i][0]) + Pn;
        Pn = Pn * (x - this._coefficients[i][0]) + this._coefficients[i][1];
    }
    return [Pn, dPn];
};
HermitePolynomial.prototype.findRoot = function (seed, tolerance, iterations) {
    tolerance = tolerance || 1e-6;
    iterations = (Number.isInteger(iterations) && iterations > 1 ? iterations : 10);
    var Px = this.derivative(seed);
    if (Math.abs(Px[0]) < tolerance) {
        return seed;
    }
    var x1 = seed, x0;
    for (var i = 0, diff = Math.abs(Px[0]); i < iterations && diff > tolerance; i += 1) {
        x0 = x1;
        Px = this.derivative(x0);
        x1 = x0 - Px[0] / Px[1];
        diff = Math.abs(x1 - x0);
    }
    if (i === iterations) {
        return null;
    }
    return x1;
};
/**
 * Constructor for spline using Hermite Interpolation
 * @param {array[float[]]} interpolationPoints [[x, f(x), f'(x), ..., f^n(x)]];
 */
function HermiteSpline(interpolPts) {
    this._interpolPts = interpolPts;
    this._interpolationPolynomials = [];
    this._resetAllPolynomials();
}
Object.defineProperties(HermiteSpline.prototype, {
    '_getIndexOfSegment': {
        enumerable: false,
        value: function (x) {
            if (this.outOfBounds(x)) {
                return null;
            }
            var index = 0;
            for (var ln = this._interpolPts.length; index < ln && this._interpolPts[index][0] < x; index += 1) {
            }
            return Math.max(0, index - 1);
        }
    },
    '_getIndexOfPoint': {
        enumerable: false,
        value: function (x) {
            if (this.outOfBounds(x)) {
                return null;
            }
            var index = 0;
            for (; x > this._interpolPts[index + 1][0]; index += 1) {
            }
            return index;
        }
    },
    '_getIndexOfPointByIndexOrPoint': {
        enumerable: false,
        value: function (indexOrPoint) {
            var index = (Number.isInteger(indexOrPoint) ? indexOrPoint : this._interpolPts.findIndex(function (item) {
                if (item.length !== indexOrPoint.length) {
                    return false;
                }
                var result = true;
                for (var i = 0, ln = indexOrPoint.length; i < ln && result; i += 1) {
                    if (indexOrPoint[i] !== item[i]) {
                        result = false;
                    }
                }
                return result;
            }));
            if (index < 0 || index > this._interpolPts.length - 1) {
                return null;
            }
            return index;
        }
    },
    '_resetAllPolynomials' : {
      enumarable : true,
      value : function () {
        var normalizedPoints = this._unrollArrayPoints(this._interpolPts);
        this._interpolationPolynomials = [];
        for (var i = 1, ln = normalizedPoints.length; i < ln; i += 1) {
          this._interpolationPolynomials.push(new HermitePolynomial(normalizedPoints[i - 1].concat(normalizedPoints[i])));
        }
      }
    },
    '_updateCoefficients': {
        enumerable: false,
        value: function (index) {
          if (index > 0 || index <= this._interpolPts.length - 1) {
            return new Error('Index out of bound.');
          }
          var unrolledPoints = this._unrollArrayPoints([
            this._interpolPts[index],
            this._interpolPts[index + 1]
          ]);
          this._interpolationPolynomials[index] = new HermitePolynomial(unrolledPoints[0].concat(unrolledPoints[1]));
        }
    },
    '_unrollArrayPoints': {
        enumerable: false,
        value: function (points) {
            return points.reduce(function (acum, point) {
                var expandedPoint = [];
                for (var i = 1, ln = point.length; i < ln; i += 1) {
                    expandedPoint.push([point[0], point[i]]);
                }
                acum.push(expandedPoint);
                return acum;
            }, []);
        }
    },
    'outOfBounds': {
        enumerable: true,
        value: function (x) {
            return (x < this._interpolPts[0][0] || x > this._interpolPts[this._interpolPts.length - 1][0] ? true : false);
        }
    },
    'addPoint': {
        enumerable: true,
        value: function (point, allowOutsideBounds) {
        if (point[0] < this._interpolPts[0][0] || point[0] > this._interpolPts[this._interpolPts.length - 1][0]) {
          if (!allowOutsideBounds) {
            return new Error('The point that you tried to add to the interpolation is outside of the initial range.');
          }
          if (point[0] < this._interpolPts[0][0]) {
            this._interpolPts.splice(0, 0, point.slice(0));
          } else {
            this._interpolPts.push(point.slice(0));
          }
        } else {
          var insertIndex = this._getIndexOfPoint(point[0]);
          this._interpolPts.splice(insertIndex + 1, 0, point.slice(0));
        }
        this._resetAllPolynomials();
      }
    },
    'deletePoint': {
        enumerable: true,
        value: function (indexOrPoint) {
            var index = this._getIndexOfPointByIndexOrPoint(indexOrPoint);
            if (index === null) {
                return null;
            }
            var deletedPoint = this._interpolPts[index];
            this._interpolPts.splice(index, 1);
            this._resetAllPolynomials();
            return deletedPoint;
        }
    },
    'editPoint': {
        enumerable: true,
        value: function (indexOrPoint, newData) {
          var index = this._getIndexOfPointByIndexOrPoint(indexOrPoint);
          if (index === null) {
              return null;
          }
          newData = newData.map(function (coordinate) {
            return parseFloat(coordinate, 10);
          });
          if (!newData.every(function (coordinate) {
            return !Number.isNaN(coordinate);
          })) {
            return null;
          }
          this._interpolPts[index] = newData;
          this._resetAllPolynomials();
        }
    },
    'bezierCubic': {
        enumerable: true,
        get : function () {
            var points = [];
            for (var i = 0, ln = this._interpolationPolynomials.length; i < ln; i += 1) {
                var multiplier = (this._interpolationPolynomials[i]._interpolPts[2][0] - this._interpolationPolynomials[i]._interpolPts[0][0]);

                var slopes = [
                  this._interpolationPolynomials[i]._interpolPts[1][1] * multiplier,
                  this._interpolationPolynomials[i]._interpolPts[3][1] * multiplier
                ];

                points.push([
                    [this._interpolationPolynomials[i]._interpolPts[0][0], this._interpolPts[0][1]],
                    [this._interpolationPolynomials[i]._interpolPts[0][0] + multiplier / 3, this._interpolationPolynomials[i]._interpolPts[0][1] + slopes[0] / 3],
                    [this._interpolationPolynomials[i]._interpolPts[2][0] - multiplier / 3, this._interpolationPolynomials[i]._interpolPts[2][1] - slopes[1] / 3],
                    [this._interpolationPolynomials[i]._interpolPts[2][0], this._interpolationPolynomials[i]._interpolPts[2][1]]
                ]);
            }
            return points;
        }
    },
    'evaluate': {
        enumerable: true,
        value: function (x) {
            var index = this._getIndexOfSegment(x);
            return (index === null ? null : this._interpolationPolynomials[index].evaluate(x));
        }
    }
});

module.exports = {
  RescaleBounds : RescaleBounds,
  HermitePolynomial : HermitePolynomial,
  HermiteSpline : HermiteSpline,
  CubicSpline : HermiteSpline
}
