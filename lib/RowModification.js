'use strict';

function RowModification(value, target, options) {
  const wrongFirstArgumentMsg = 'Constructor requires a number as a first argument or an object with a number in terminalValue property';
  const missingTargetMsg = 'Missing target or not a string';
  const {HermiteSpline} = require('./LinearAlgebra.js');

  this.value = {
    terminalValue : (
      value && (value.terminalValue === 0 || value.terminalValue) ?
      Number.parseFloat(value.terminalValue) :
      Number.parseFloat(value)
    ),
    timeSeries : null,
    spread : null,
    startTime : (value && value.startTime ? value.startTime : null)
  };

  if (Number.isNaN(this.value.terminalValue)) {
    throw new TypeError(wrongFirstArgumentMsg);
  }

  if (value && Array.isArray(value.timeSeries)) {
    this.value.timeSeries = new HermiteSpline(value.timeSeries);
  }

  if (value && value.spread) {
    this.value.spread = value.spread;
  }


  if (target && (typeof target === 'string' || target instanceof String)) {
    this.target = target;
  } else {
    throw new TypeError(missingTargetMsg);
  }

  this.isRelative = (options && options.isRelative === false ? false : true);
  this.timeColumn = (options && options.timeColumn ? options.timeColumn : null);
  this.geoColumn = (options && options.geoColumn ? options.geoColumn : null);

  this.geoIdToCodes = (options && !!(options.geoIdToCodes &&
    options.geoIdToCodes.constructor && options.geoIdToCodes.call &&
    options.geoIdToCodes.apply) ?
    options.geoIdToCodes :
    (code) => { return code; }
  );

  if (options && options.filter) {
    this.filter = Object.keys(options.filter).reduce((acum, key) => {
      acum[key] = new Set(options.filter[key]);
      return acum;
    }, {});
  } else {
    this.filter = null;
  }

}

Object.defineProperties(RowModification.prototype, {
  '__modifyRow' : {
    enumerable : false,
    value : function (row) {
      for (let column in this.filter) {
        if (!this.filter[column].has(row[column])) {
          return false;
        }
      }
      return true;
    }
  },
  'exec' : {
    enumerable : true,
    value : function (row) {
      const {Rescale, TimeAndGeoRescale} = require('./RescaleFunctions.js');
      if (typeof row[this.target] === 'undefined') {
        throw new TypeError('The provided row does not has the target column');
      }

      const timeAndGeo = () => {
        const regionId = this.geoIdToCodes(row[this.geoColumn]);
        const timeValue = row[this.timeColumn];
        const regionData = (this.value.spread.modifiedNodes ?
          this.value.spread.modifiedNodes.get(this.geoIdToCodes(regionId)) :
          false
        );

        if (regionData) {
          if (regionData.timeSeries) {
            return regionData.timeSeries.evaluate(timeValue)
          }
          return regionData.terminalValue;
        }

        if (this.value.spread.trie && this.value.spread.AffineMap && this.value.spread.AffineMap.length) {
          const timeDependantRescale = new TimeAndGeoRescale(this.value.timeSeries, this.value.spread.trie);
          return timeDependantRescale.rescale(timeValue, regionId, ...this.value.spread.AffineMap);
        }
        return this.value.timeSeries.evaluate(timeValue);
      };

      const timeOnly = () => {
        return this.value.timeSeries.evaluate(row[this.timeColumn]);
      };

      const geoOnly = () => {
        const regionId = this.geoIdToCodes(row[this.geoColumn]);

        const regionData = (this.value.spread.modifiedNodes ?
          this.value.spread.modifiedNodes.get(regionId) :
          false
        );

        if (regionData) {
          return regionData.terminalValue;
        }

        if (this.value.spread.trie) {
          const regionValue = this.value.spread.trie.get(regionId);

          if (regionValue !== false &&
            this.value.spread.AffineMap && this.value.spread.AffineMap.length
          ) {
            return Rescale.rescale(regionValue, ...this.value.spread.AffineMap);
          } else {
            return regionValue;
          }
        }
        return this.value.terminalValue;
      };

      if (this.__modifyRow(row)) {
        let computedValue;
        const hasTime = (this.timeColumn && typeof row[this.timeColumn] !== 'undefined');
        const hasGeo = (this.geoColumn && typeof row[this.geoColumn] !== 'undefined' && this.value.spread);

        if (hasTime && this.value.startTime !== null && parseInt(this.value.startTime, 10) > parseInt(row[this.timeColumn], 10)) {
          return row;
        }

        if (hasTime && this.value.timeSeries) {
          if (hasGeo) {
            computedValue = timeAndGeo();
          } else {
            computedValue = timeOnly();
          }
        } else {
          if (hasGeo) {
            computedValue = geoOnly();
          } else {
            computedValue = this.value.terminalValue;
          }
        }
        row[this.target] = (this.isRelative ? row[this.target] * computedValue : computedValue);
      }
      return row;
    }
  }
});

module.exports = RowModification;
