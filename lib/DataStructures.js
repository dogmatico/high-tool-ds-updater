'use strict';

class Trie {
  constructor(iterable, insertionOrderPointer) {
    Object.defineProperty(this, 'value', {
      'enumerable' : false,
      'writable' : true
    });

    Object.defineProperty(this, '__insertionPointer', {
      'enumerable' : false,
      'writable' : false,
      value : insertionOrderPointer || []
    });

    Object.defineProperty(this, '__insertionIndex', {
      'enumerable' : false,
      'writable' : false,
      value : this.__insertionPointer.length
    });

    if (iterable && typeof iterable[Symbol.iterator] === 'function') {
      for (let keyVal of iterable) {
        this.set(keyVal[0], keyVal[1]);
      }
    }

    this[Symbol.iterator] = function* () {
      function* recursion(path, node) {
        if (typeof node.value !== 'undefined') {
          yield [path, node.value];
        }
        for (let l in node) {
          yield* recursion(path + l, node[l]);
        }
      }

      for (let l in this) {
        yield* recursion(l, this[l]);
      }
      return;
    }
  }

  clear() {
    Object.keys(this).forEach(key => {
      delete this[key];
    });
  }

  delete(uuid) {
    throw new Error('Not implemented');
  }

  entries() {
    return this[Symbol.iterator]()
  }

  get(uuid) {
    let node = this;
    for (let l of uuid) {
      node = node[l];
      if (typeof node === 'undefined') {
        return false;
      }
    }
    return (node && typeof node.value !== 'undefined' ? node.value : false);
  }

  has(uuid) {
    let obj = this.get(uuid);
    return !!obj;
  }

  keys() {
    return (function*() {
      function* recursion(path, node) {
        if (typeof node.value !== 'undefined') {
          yield path;
        }
        for (let l in node) {
          yield* recursion(path + l, node[l]);
        }
      }

      for (let l in this) {
        yield* recursion(l, this[l]);
      }
      return;
    }.bind(this)());
  }

  set(uuid, value) {
    let node = this;
    for (let l of uuid) {
      node = node[l] || (node[l] = new Trie([], this.__insertionPointer));
      node.__insertionPointer.push(node)
    }
    node.value = value;
    return node.value;
  }

  values() {
    return this.__insertionPointer.map(item => item.value).filter(item => item !== undefined);
  }
}

class HierarchicalTrie extends Trie {
  constructor(rootNodeId, iterable, options = {}) {
    super(iterable);
    this.rootNode = super.get.call(this, rootNodeId);
    if (this.rootNode === false && options.disconnected !== true) {
      throw new Error('Undefined root node. Add it to the first argument.');
    }
  }

  get(uuid) {
    let lastFound = this.rootNode;
    let node = this;
    for (let l of uuid) {
      node = node[l];
      if (typeof node === 'undefined') {
        return lastFound;
      } else if (typeof node.value !== 'undefined') {
        lastFound = node.value;
      }
    }
    return lastFound;
  }
}

module.exports = {
  Trie : Trie,
  HierarchicalTrie : HierarchicalTrie
};
