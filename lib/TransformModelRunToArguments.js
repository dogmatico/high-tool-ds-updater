'use strict';

function transformModelRunToArguments(modelRunParameter, targetDimensionsDict = {}, validTries = {}) {
  const {Trie, HierarchicalTrie} = require('./DataStructures.js');
  const {HermiteSpline} = require('./LinearAlgebra.js');
  const rootNodeId = 'EU28';
  const retObject = {
    value : {},
    target : '',
    options : {}
  }

  if (!modelRunParameter || !modelRunParameter.target || !Array.isArray(modelRunParameter.target.__parameters) || modelRunParameter.target.__parameters.length !== 1) {
    throw new TypeError('ModelRuns must have include an array in target.__parameters with a single element');
  }

  if (typeof modelRunParameter.target.__parameters[0] !== 'string' || modelRunParameter.target.__parameters[0].length === 0) {
    throw new TypeError('Wrong arguments: target.__parameters[0] must be a non-empty string');
  }
  retObject.target = modelRunParameter.target.__parameters[0];

  if (typeof modelRunParameter.target.__is_relative !== 'undefined') {
    retObject.options.isRelative = modelRunParameter.target.__is_relative;
  }

  const validGeoColumns = new Set([
    'ht2006_level_2',
    'region_eu',
    'region',
    'country'
  ]);

  const validTimeColumns = new Set([
    'time',
    'year'
  ]);

  retObject.value.startTime = (modelRunParameter.values && modelRunParameter.values.startYear ? modelRunParameter.values.startYear : 2015);

  if (targetDimensionsDict[retObject.target]) {
    for (let timeColumn of validTimeColumns.values()) {
      if (targetDimensionsDict[retObject.target].dimensions.has(timeColumn)) {
        retObject.options.timeColumn = timeColumn + '_id';
      }
    }

    for (let geoColumn of validGeoColumns.values()) {
      if (targetDimensionsDict[retObject.target].dimensions.has(geoColumn)) {
        retObject.options.geoColumn = geoColumn + '_id';
      }
    }
  }

  if (modelRunParameter.target.__dimensions) {
    retObject.options.filter = {};
    for (let key in modelRunParameter.target.__dimensions) {
      retObject.options.filter[key + '_id'] = [...modelRunParameter.target.__dimensions[key]];
    }
  }

  if (Number.isFinite(modelRunParameter.values.terminalValue)) {
    retObject.value.terminalValue = modelRunParameter.values.terminalValue;
  } else {
    throw new TypeError('Missing values. Cannot transform the given modelRun.');
  }

  if (modelRunParameter.values.representationaData &&
    modelRunParameter.values.representationaData.timeSeries &&
    modelRunParameter.values.representationaData.timeSeries.length &&
    Array.isArray(modelRunParameter.values.representationaData.timeSeries)
  ) {
    retObject.value.timeSeries = [...modelRunParameter.values.representationaData.timeSeries];
  }

  if (modelRunParameter.values.representationaData.spread &&
    Object.keys(modelRunParameter.values.representationaData.spread).length
  ) {
    retObject.value.spread = {};

    if (modelRunParameter.values.representationaData.spread.id !== 'baseline') {
      if (validTries[modelRunParameter.values.representationaData.spread.id]) {
        retObject.value.spread.trie = new HierarchicalTrie(rootNodeId, validTries[modelRunParameter.values.representationaData.spread.id]);
      }

      if (modelRunParameter.values.representationaData.spread.transformation &&
        Array.isArray(modelRunParameter.values.representationaData.spread.transformation) &&
        modelRunParameter.values.representationaData.spread.transformation.length
      ) {
        retObject.value.spread.AffineMap = [...modelRunParameter.values.representationaData.spread.transformation];
      }
    }

    if (modelRunParameter.values.representationaData.spread.modifiedNodes &&
      Array.isArray(modelRunParameter.values.representationaData.spread.modifiedNodes) &&
      modelRunParameter.values.representationaData.spread.modifiedNodes.length
    ) {

      const modifiedNodes = modelRunParameter.values.representationaData.spread.modifiedNodes.map(item => {
        const {value, timeSeries} = item;
        const objVal = {
          terminalValue : value
        };

        if (timeSeries && timeSeries.length) {
          objVal.timeSeries = new HermiteSpline([...timeSeries]);
        }
        return [item.key, objVal];
      });

      retObject.value.spread.modifiedNodes = new HierarchicalTrie(rootNodeId, modifiedNodes, {
        disconnected : true
      });
    }

  }

  return retObject;
}

module.exports = transformModelRunToArguments;
