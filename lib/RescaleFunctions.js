class Rescale {
  constructor () {
  }

  static rescale(x, min, max, lowerBound = 0, upperBound = 1, tol = 1e-8) {
    if (arguments.length < 3) {
      throw new TypeError('Missing arguments');
    }
    if (x < min || x > max) {
      throw new TypeError('Out of bounds argument');
    }
    if (Math.abs(lowerBound - upperBound) < tol) {
      return lowerBound;
    }
    return lowerBound + (x - min)*(upperBound - lowerBound)/(max - min);
  }
}

class TimeAndGeoRescale extends Rescale {
  constructor (referenceTrajectory, hierarchicalTrie) {
    if (!referenceTrajectory) {
      throw new TypeError('Provide a reference trajectory as the first argument of the constructor')
    }
    if (!hierarchicalTrie) {
      throw new TypeError('Provide a HierarchicalTrie as the second argument of the constructor')
    }
    super();
    Object.defineProperties(this, {
      '__trie' : {
        'enumerable' : false,
        'writable ' : false,
        'value' : hierarchicalTrie
      },
      '__referenceTrajectory' : {
        'enumerable' : false,
        'writable' : false,
        'value' : referenceTrajectory
       }
    });
  }

  rescale(time, geo, minX, maxX, minY, maxY) {
    const regionValue = this.__trie.get(geo);
    const refVal = this.__referenceTrajectory.evaluate(time);
    const [superMin, superMax, superLowB, superUpB] = (regionValue < refVal ? [minX, refVal, minY, refVal] : [refVal, maxX, refVal, maxY]);
    return Rescale.rescale(regionValue, superMin, superMax, superLowB, superUpB);
  }
}

module.exports = {
  Rescale : Rescale,
  TimeAndGeoRescale : TimeAndGeoRescale
};
