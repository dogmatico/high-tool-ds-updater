# HIGH-TOOL Data Stock Updater
This module transforms a Data Stock Row to match a lever action using the class
*RowModification*.

Includes a method, *transformModelRunToArguments*, to transform HTModelRuns
to the arguments needed by *RowModification*'s constructor.

## Testing
Unit tested using Jasmine. Run
```bash
npm run test
```
